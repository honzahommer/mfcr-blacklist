#!/bin/bash

DIR="$(dirname $0)/.."

SRC_DIR="$DIR"
SRC_LIST="$SRC_DIR/blacklist.txt"

wget -qO- https://csnog.github.io/MFCR-blacklist/blacklist.txt | awk 'NF > 0 { print tolower($0) }' > $SRC_LIST
