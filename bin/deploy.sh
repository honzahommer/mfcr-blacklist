#!/bin/bash

has() {
  type "$1" > /dev/null 2>&1
}

checksum() {
  if has "md5"; then
    md5 "$i" | awk '{ print $NF }'
  elif has "md5sum"; then
    md5sum "$i" | cut -d' ' -f1
  fi
}

DIR="$(dirname $0)/.."

SRC_DIR="$DIR"
SRC_LIST="$SRC_DIR/blacklist.txt"

DIST_DIR="$DIR/public"
DIST_ZONES="$DIST_DIR/zones.mfcr-blacklist"
DIST_EMPTY="$DIST_DIR/empty.db"
DIST_LIST="$DIST_DIR/blacklist.txt"
DIST_INDEX="$DIST_DIR/index.html"

if [ ! -d "$DIST_DIR" ]; then
  mkdir -p "$DIST_DIR"
fi

cp "$SRC_LIST" "$DIST_LIST"

cat "$SRC_LIST" | while read i; do
  echo "zone \"$i\" { type master; notify no; file \"empty.db\"; };"
done > "$DIST_ZONES"

cat <<EOT > "$DIST_EMPTY"
\$TTL	86400
@	IN	SOA	localhost. root.localhost. (
			      1		; Serial
			 604800		; Refresh
			  86400		; Retry
			2419200		; Expire
			  86400 )	; Negative Cache TTL
;
@	IN	NS	localhost.
EOT

for i in "$DIST_LIST" "$DIST_ZONES" "$DIST_EMPTY"; do
  checksum "$i" > "$i.md5"
done

cat <<EOT > "$DIST_INDEX"
<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <title>$CI_PROJECT_NAME</title>
  </head>
  <body>
    <nav class="navbar navbar-dark bg-primary">
      <a class="navbar-brand" href="$CI_PROJECT_URL">$CI_PROJECT_NAME</a>
    </nav>

    <div class="container-fluid mt-4">
      <ul>$(find "$DIST_DIR" ! -name "$(basename "$DIST_INDEX")" -type f -exec basename {} \; | sort | awk '{ printf "<li><a href=\"%s\">%s</a></li>", $0, $0 }')</ul>
      <p><small class="text-muted">Last Update: $(date)</small></p>
    </div>
  </body>
</html>
EOT
